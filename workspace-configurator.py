#!/usr/bin/python3
import npyscreen as npy
import os
import yaml
import subprocess
import time
class Handler(object):
    def __init__(self):
        try:
            self.file = open(os.path.expanduser("~/.config/workspace-configurator/config.yaml"), "r")
            self.yamlfile = yaml.safe_load(self.file)
        except:
            try:
                os.copyfile("/etc/workspace-configurator.yaml", os.path.expanduser("~/.config/workspace-configurator/config.yaml"))
            except:
                exit("Config file cannot be found")
    def getconfig(self):
        v = []
        try:
            for i in self.yamlfile:
                v.append(i)
            return(v)
        except:
            exit("Empty file")
    def getvalues(self, confid):
        v = []
        try:
            for i in self.yamlfile[confid]:
                v.append(i)
            return(v)
        except:
            exit("Empty file")
    def window_spawn(self, app, ws):
        bad = ["discord", "steam"]
        cmd = "xdotool set_desktop "+str(ws)
        subprocess.Popen(["xdotool", "set_desktop", str(ws)])
        subprocess.Popen(app)
        if app in bad:
            time.sleep(8)
        else:
            time.sleep(2)
    def spawner(self, numid):
        confid = list(self.yamlfile)[numid]
        for i in self.yamlfile[confid]:
            for x in self.yamlfile[confid][i]:
                self.window_spawn(x, i)
class Edit(npy.ButtonPress):
    def whenPressed(self):
            edit = os.getenv("EDITOR")
            os.system("/bin/" + edit + os.path.expanduser(" ~/.config/workspace-configurator/config.yaml"))
            npy.blank_terminal()
            self.parent.parentApp.main.DISPLAY()
            npy.blank_terminal()


class InputForm(npy.ActionFormV2):
    def beforeEditing(self):
        self.combo.values = self.parentApp.handler.getconfig()
    def afterEditing(self):
        self.parentApp.setNextForm(None)
    def create(self):
        x, y = self.useable_space()
        self.combo = self.add(npy.TitleCombo, name='Choose configuration')
        self.Edit = self.add(Edit, name="Edit config")
    def on_cancel(self):
        self.parentApp.setNextForm(None)
    def on_ok(self):
        if self.combo.value == None:
            self.display_error("Please choose a configuration")
        else:
            self.parentApp.handler.spawner(self.combo.value)
    def display_error(self, msg):
        npy.blank_terminal()
        npy.notify(msg, title="Error")
        time.sleep(1)
        self.display()
        self.edit()
class Application(npy.NPSAppManaged):
   def onStart(self):
       self.main = self.addForm('MAIN', InputForm, name='Workspace configurator')
       self.handler = Handler()

if __name__ == '__main__':
   App = Application().run()
